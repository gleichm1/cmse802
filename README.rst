=======
cmse802
=======

.. image:: https://img.shields.io/travis//cmse802.svg
        :target: https://travis-ci.org//cmse802

.. image:: https://img.shields.io/pypi/v/cmse802.svg
        :target: https://pypi.python.org/pypi/cmse802


Python package for doing science.

* Free software: 3-clause BSD license
* Documentation: (COMING SOON!) https://.github.io/cmse802.

Features
--------

* TODO
