=======
Credits
=======

Maintainer
----------

* Adam Gleichman <gleichm1@msu.edu>

Contributors
------------

None yet. Why not be the first? See: CONTRIBUTING.rst
